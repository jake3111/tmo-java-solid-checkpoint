package com.galvanize.formatters;

import com.galvanize.Calendar;

public interface Formatter{
    public String format(Calendar calendar);
}
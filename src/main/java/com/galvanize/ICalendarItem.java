package com.galvanize;

import java.time.format.DateTimeFormatter;
import java.util.UUID;

public abstract class ICalendarItem implements Displayable {

    public static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MMM d, y h:mm a");
    protected String content;
    private String uuid;

    public ICalendarItem() {
        super();
        uuid = UUID.randomUUID().toString();
    }

    protected abstract String getContent();

    protected String getUuid() {
        return uuid;
    }

    public abstract String iCalendar();

}

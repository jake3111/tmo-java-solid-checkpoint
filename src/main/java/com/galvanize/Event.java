package com.galvanize;

import java.time.Duration;
import java.time.LocalDateTime;

public class Event extends ICalendarItem implements Schedulable {

    private final LocalDateTime startsAt;
    private final Duration duration;

    public Event(String title, LocalDateTime startsAt, Duration duration) {
        this.content = title;
        this.startsAt = startsAt;
        this.duration = duration;
    }

    public String getTitle() {
        return content;
    }

    public String getContent() {
        return content;
    }

    public String getTextToDisplay() {
        return getTitle();
    }

    @Override
    public LocalDateTime getTimeAt() {
        return startsAt;
    }

    public Duration getDuration() {
        return duration;
    }

    public LocalDateTime getEndsAt() {
        return startsAt.plus(getDuration());
    }

    @Override
    public String iCalendar() {
        if (content == null) return "";

        return new StringBuilder()
                .append("BEGIN:VEVENT\n")
                .append(String.format("DTSTART:%s\n", getTimeAt()))
                .append(String.format("DTEND:%s\n", getEndsAt()))
                .append(String.format("UID:%s@example.com\n", getUuid()))
                .append(String.format("DESCRIPTION:%s\n", getTextToDisplay()))
                .append("END:VEVENT\n")
                .toString();
    }

    @Override
    public String toString() {
        return String.format(
                "%s at %s (ends at %s)",
                getTitle(),
                getTimeAt().format(DATE_FORMATTER),
                getEndsAt().format(DATE_FORMATTER)
        );
    }
}

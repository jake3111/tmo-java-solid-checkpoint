package com.galvanize;

import java.time.LocalDateTime;

public interface Schedulable {
    public LocalDateTime getTimeAt();
}
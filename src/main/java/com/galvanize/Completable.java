package com.galvanize;

import java.time.LocalDateTime;

public interface Completable extends Displayable{

    void markComplete();

    void markIncomplete();

    boolean isComplete();

    LocalDateTime getLocalDateTime();

}
